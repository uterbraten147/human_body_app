﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject PanelPrincipal, PanelCreditos, PanelExtras;
    // Start is called before the first frame update
    void Start()
    {
        PanelPrincipal.SetActive(true);
        PanelCreditos.SetActive(false);
        PanelExtras.SetActive(false);
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void Creditos()
    {
        PanelPrincipal.SetActive(false);
        PanelCreditos.SetActive(true);
        PanelExtras.SetActive(false);
    }

    public void Regresar()
    {
        PanelPrincipal.SetActive(true);
        PanelCreditos.SetActive(false);
        PanelExtras.SetActive(false);
    }

    public void Extras()
    {
        PanelPrincipal.SetActive(true);
        PanelCreditos.SetActive(false);
        PanelExtras.SetActive(true);
    }

    public void Enlace()
    {
        Application.OpenURL("https://drive.google.com/drive/folders/1-82JhoVLLk3NWl049Q-MP__6l6Pc0pYN?usp=sharing");
    }



    public void VR_Scene()
    {

    }

    public void AR_Scene()
    {
        SceneManager.LoadScene(1);
        
    }
}
