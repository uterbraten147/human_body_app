// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-9803-OUT,clip-2944-OUT;n:type:ShaderForge.SFN_Tex2d,id:8049,x:31618,y:33186,ptovrint:False,ptlb:Main_Tex,ptin:_Main_Tex,varname:node_8049,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:315f4922bbb1ed94287bab88dc7305fc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_NormalVector,id:1540,x:31290,y:32824,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:6785,x:31290,y:32970,varname:node_6785,prsc:2;n:type:ShaderForge.SFN_Dot,id:1471,x:31459,y:32883,varname:node_1471,prsc:2,dt:0|A-1540-OUT,B-6785-OUT;n:type:ShaderForge.SFN_Multiply,id:9803,x:31928,y:33106,varname:node_9803,prsc:2|A-8549-OUT,B-8049-RGB;n:type:ShaderForge.SFN_RemapRange,id:8549,x:31627,y:32922,varname:node_8549,prsc:2,frmn:0,frmx:1,tomn:0.5,tomx:1|IN-1471-OUT;n:type:ShaderForge.SFN_Time,id:6438,x:31628,y:33413,varname:node_6438,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4488,x:31804,y:33466,varname:node_4488,prsc:2|A-6438-T,B-7499-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7499,x:31611,y:33572,ptovrint:False,ptlb:time_speed,ptin:_time_speed,varname:node_7499,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_RemapRange,id:9750,x:31957,y:33466,varname:node_9750,prsc:2,frmn:-0.9,frmx:0.9,tomn:0,tomx:1|IN-4488-OUT;n:type:ShaderForge.SFN_Sin,id:9579,x:32117,y:33466,varname:node_9579,prsc:2|IN-9750-OUT;n:type:ShaderForge.SFN_TexCoord,id:3779,x:31839,y:33683,varname:node_3779,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Step,id:7973,x:32321,y:33532,varname:node_7973,prsc:2|A-9579-OUT,B-3779-U;n:type:ShaderForge.SFN_SwitchProperty,id:2944,x:32416,y:33233,ptovrint:False,ptlb:blinks,ptin:_blinks,varname:node_2944,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3337-OUT,B-8105-OUT;n:type:ShaderForge.SFN_Vector1,id:3337,x:32146,y:33259,varname:node_3337,prsc:2,v1:1;n:type:ShaderForge.SFN_SwitchProperty,id:8105,x:32499,y:33645,ptovrint:False,ptlb:flip,ptin:_flip,varname:node_8105,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-7456-OUT,B-8119-OUT;n:type:ShaderForge.SFN_OneMinus,id:8119,x:32326,y:33695,varname:node_8119,prsc:2|IN-7456-OUT;n:type:ShaderForge.SFN_Smoothstep,id:7456,x:32302,y:33819,varname:node_7456,prsc:2|A-3583-OUT,B-3939-OUT,V-9579-OUT;n:type:ShaderForge.SFN_Vector1,id:3583,x:32100,y:33828,varname:node_3583,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Vector1,id:3939,x:32073,y:33872,varname:node_3939,prsc:2,v1:0.9;proporder:8049-7499-2944-8105;pass:END;sub:END;*/

Shader "Shader Forge/UnlitBody" {
    Properties {
        _Main_Tex ("Main_Tex", 2D) = "white" {}
        _time_speed ("time_speed", Float ) = 10
        [MaterialToggle] _blinks ("blinks", Float ) = 1
        [MaterialToggle] _flip ("flip", Float ) = 0.4614569
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma target 3.0
            uniform sampler2D _Main_Tex; uniform float4 _Main_Tex_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _time_speed)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _blinks)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _flip)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float4 node_6438 = _Time;
                float _time_speed_var = UNITY_ACCESS_INSTANCED_PROP( Props, _time_speed );
                float node_9579 = sin(((node_6438.g*_time_speed_var)*0.5555556+0.5));
                float node_7456 = smoothstep( 0.1, 0.9, node_9579 );
                float _flip_var = lerp( node_7456, (1.0 - node_7456), UNITY_ACCESS_INSTANCED_PROP( Props, _flip ) );
                float _blinks_var = lerp( 1.0, _flip_var, UNITY_ACCESS_INSTANCED_PROP( Props, _blinks ) );
                clip(_blinks_var - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
////// Emissive:
                float4 _Main_Tex_var = tex2D(_Main_Tex,TRANSFORM_TEX(i.uv0, _Main_Tex));
                float3 emissive = ((dot(normalDirection,lightDirection)*0.5+0.5)*_Main_Tex_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma target 3.0
            uniform sampler2D _Main_Tex; uniform float4 _Main_Tex_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _time_speed)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _blinks)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _flip)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float4 node_6438 = _Time;
                float _time_speed_var = UNITY_ACCESS_INSTANCED_PROP( Props, _time_speed );
                float node_9579 = sin(((node_6438.g*_time_speed_var)*0.5555556+0.5));
                float node_7456 = smoothstep( 0.1, 0.9, node_9579 );
                float _flip_var = lerp( node_7456, (1.0 - node_7456), UNITY_ACCESS_INSTANCED_PROP( Props, _flip ) );
                float _blinks_var = lerp( 1.0, _flip_var, UNITY_ACCESS_INSTANCED_PROP( Props, _blinks ) );
                clip(_blinks_var - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float3 finalColor = 0;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma target 3.0
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _time_speed)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _blinks)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _flip)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                float4 node_6438 = _Time;
                float _time_speed_var = UNITY_ACCESS_INSTANCED_PROP( Props, _time_speed );
                float node_9579 = sin(((node_6438.g*_time_speed_var)*0.5555556+0.5));
                float node_7456 = smoothstep( 0.1, 0.9, node_9579 );
                float _flip_var = lerp( node_7456, (1.0 - node_7456), UNITY_ACCESS_INSTANCED_PROP( Props, _flip ) );
                float _blinks_var = lerp( 1.0, _flip_var, UNITY_ACCESS_INSTANCED_PROP( Props, _blinks ) );
                clip(_blinks_var - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
